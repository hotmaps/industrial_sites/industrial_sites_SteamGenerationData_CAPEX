[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687145.svg)](https://doi.org/10.5281/zenodo.4687145)

# Industrial Sites: Steam Generation Data - CAPEX 

In this repository is published the Capital Expenditures (CAPEX) for industrial steam generation technologies.


## Repository structure

Files:
```
data/SteamGenerationData_CAPEX.csv                          -- Capital expenditures (CAPEX) for industrial steam generation technologies
datapackage.json                                            -- Datapackage JSON file with the main meta-data

```

## Documentation
The dataset in the folder "industrial_sites" analyses the industrial Heating and Cooling energy demand and comprises of three mail elements:

**Table 1.** Characteristics of data provided within Task 2.4 Industrial Processes.
<table>
   <tr>
    <td>Task</td>
    <td>Dataset</td>
  </tr>
  <tr>
    <td>1. Performance and cost data of industrial steam and district heating generation technologies</td>
    <td>CAPEX, OPEX, Lifetime, ThermalEfficiency, PowertoHeatRatio</td>
  </tr>
  <tr>
    <td>2. Benchmarks on H&C Final Energy Consumption (FEC) and excess heat potentials for industrial processesIndustryBenchmarks</td>
    <td>IndustryBenchmarks</td>
  </tr>
  <tr>
    <td>3. Industrial plants FEC and excess heat potentials</td>
    <td>IndustrialDatabase</td>
  </tr>
</table>

The datasets are to be found in the respective repository.

## Description of the task
The objective of this subtask is to develop a consistent EU-wide performance and cost dataset for industrial steam and hot water and DH generation technologies. 
This dataset shall provide the basis for modelling of technology choices.
Industrial steam and hot water can be provided by a variety of different technologies including combined heat and power but also classical separate generation of heat in boilers. 
For the lower temperature range, HPs and DH can be used. 
In order to provide a comprehensive set required for technology choices, the following technologies are included in the dataset.

**Table 2.** Technologies included in this dataset.
<table>
  <tr>
    <td>Technology</td>
    <td>CHP</td>
    <td>Energy Carrier</td>
  </tr>
<tr>
    <td>Internal combustion engine</td>
    <td>	Yes</td>
    <td>	Natural gas
<tr>
    <td>Internal combustion engine</td>
    <td>	Yes</td>
    <td>	Liquid biofuels</td>
  </tr>
<tr>
    <td>Internal combustion engine</td>
    <td>	Yes</td>
    <td>	Light fuel oil</td>
  </tr>
<tr>
    <td>Gas turbine</td>
    <td>	Yes</td>
    <td>	Natural gas, process gases</td>
  </tr>
<tr>
    <td>Steam turbine</td>
    <td>	Yes</td>
    <td>	Light fuel oil</td>
  </tr>
<tr>
    <td>Steam turbine</td>
    <td>	Yes</td>
    <td>	Biomass</td>
  </tr>
<tr>
    <td>Steam turbine</td>
    <td>	Yes</td>
    <td>	Natural gas</td>
  </tr>
<tr>
    <td>Steam turbine</td>
    <td>	Yes</td>
    <td>	Hard coal</td>
  </tr>
<tr>
    <td>Steam turbine</td>
    <td>	Yes</td>
    <td>	Waste</td>
  </tr>
<tr>
    <td>Combined cycle gas turbine</td>
    <td>	Yes</td>
    <td>	Natural gas, process gases</td>
  </tr>
<tr>
    <td>Fuel cell</td>
    <td>	Yes</td>
    <td>	Natural gas</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Natural gas</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Hard coal</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Light fuel oil</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Biomass</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Other</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Waste</td>
  </tr>
<tr>
    <td>Boiler</td>
    <td>	No</td>
    <td>	Electricity</td>
  </tr>
<tr>
    <td>District heat</td>
    <td>	No</td>
    <td>	-</td>
  </tr>
<tr>
    <td>Heat pump sorption</td>
    <td>	No</td>
    <td>	Natural gas, other</td>
  </tr>
<tr>
    <td>Heat pump compression</td>
    <td>	No</td>
    <td>	Electricity</td>
  </tr>
<tr>
    <td>Solar district heat</td>
    <td>	No</td>
    <td>	-</td>
  </tr>
<tr>
    <td>Geothermal heat pump</td>
    <td>	No</td>
    <td>	Electricity</td>
  </tr>
</table>

The techno-economic data derived for each technology consists of the following elements:
-	Capital expenditures (CAPEX);
-	Operation and maintenance expenditures (OPEX);
-	Efficiency (thermal);
-	Power-to-heat ratio;
-   Lifetime.

### Methodology

A challenge in developing an EU-wide data set is the very low availability of empirical data for such technologies. 
Steam boilers and even more CHP units are tailor made installations. Price cataloguers are not available. 
Consequently, literature values were collected and combined for individual technologies. 
Based on a small set of collected data points, the complete set of technology performance data was calculated for all countries. 
This requires assumptions on cost differences between countries as well as functional relations between e.g. the installed power and the specific capital expenditures.
The parameters and formulas used as well as a comprehensive selection of results are available in the online dataset.  
The data for CAPEX is available in € in dependence of kW<sub>th</sub> and is provided in following resolution:

<table>
  <tr>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>NUTS0</td>
    <td>yearly (2015, 2030, 2050)</td>
  </tr>
</table>

### Limitations of data

Limitations of the data set are mainly related to uncertainty, which is estimated to be relatively high, due to two main reasons. 
First, the empirical foundation of the dataset is small, with only selected literature values for individual countries, technologies 
and capacities. The empirical data is not sufficient to benchmark and validate the method used to transform data across countries. 
Further, only little is known about the stock of steam generation technologies in Europe.
Compared to SH technologies, this is still an unexplored field, but with a very high relevance in terms of energy demand.

## References
[1]	[Technologische und energiepolitische Bewertung der Perspektiven von Kraft-Wärme-Kopplung in Deutschland](https://www.ensys.tu-berlin.de/fileadmin/fg8/Downloads/Sonstiges/2010_KWK_Studie_Langversion_FGEnsys_TUBerlin.pdf) 
G. Erdmann and L. Dittmar, 2010.  
[2]	[Catalog of CHP Technologies](https://www.epa.gov/sites/production/files/2015-07/documents/catalog_of_chp_technologies.pdf) 
K. Darrow, R. Tidball, J. Wang, and A. Hampson, 2015.  
[3]	[Zur Wirtschaftlichkeit von Kraft-Wärme-Kopplungs-Anlagen. Band 1: Der KWK-Wirtschaftlichkeitsindikator COGIX – Methodenband](https://www.bhkw-infozentrum.de/download/Anlage_KWK-Wirtschaftlichkeit_110216.pdf)
F. Matthes and H. J. Ziesing, 2016.  
[4]	[Combined Heat and Power](https://iea-etsap.org/E-TechDS/PDF/E04-CHP-GS-gct_ADfinal.pdf)
P. Lako, Energy Agency - Energy Technology Systems Analysis Programme (IEA ETSAP), 2010.  
[5]	[Mapping and analyses for the current and future (2020 - 2030) heating/cooling fuel development (fossil/renewables)](https://www.isi.fraunhofer.de/content/dam/isi/dokumente/ccx/mapping-eu/Mapping-HC-ExcecutiveSummary.pdf) 
Fraunhofer ISI, Fraunhofer ISE, TU Wien, TEP Energy, IREES, Observer, 2017.

## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 

### Authors
Tobias Fleiter, Pia Manz <sup>*</sup>

<sup>*</sup> [Fraunhofer Institute for Systems and Innovation Research](https://www.isi.fraunhofer.de/en.html)  
Breslauer Str. 48  
D-76139 Karlsruhe

### License

Copyright © 2016-2018: Tobias Fleiter, Pia Manz
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) 
(Grant Agreement number 723677), which provided the funding to carry out the present investigation.